package day9work;
//first main thread will execute then the other threads will race among themselves to complete their wrk
public class OneObjectTwoThreads {
	public static void main(String[] args) {
		ReservationCounter r=new ReservationCounter();
		new Thread(()->{
			synchronized(r) {
			r.bookTicket(1000);
			r.giveChange();}
		},"ramu").start();	
		new Thread(()->{
			synchronized(r) {
			r.bookTicket(500);
			r.giveChange();}
//			r.drinkWater();
		},"somu").start();	
	}
}
class ReservationCounter{
	int amt;
	 public void bookTicket(int amt) {
		this.amt=amt;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Ticket booked for..:"+name+" and amt paid is..:"+amt);
	
//		try {
//			Thread.sleep(10000);
//		}catch(Exception e) {}

	}
	synchronized public void giveChange() {
		int change=amt-100;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Change given to  :"+name+" and the change given is..:"+change);
	}
	public void drinkWater() {
		System.out.println("Drinking water..");
	}
}