package day9work;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecuterDemo {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService es=Executors.newFixedThreadPool(3);
		es.execute(()->{
			System.out.println("Child thread");
		});
		es.execute(()->{
			System.out.println(new MyRunnableJob());
		});
		Future future=es.submit(new MyCallableJob());
		System.out.println("value returned:"+future.get());
		System.out.println("main thread");
		es.shutdown();
	}

}
class MyRunnableJob implements Runnable{
	@Override
	public void run() {
		System.out.println("i am void void");
		
	}
}
class MyCallableJob implements Callable{
	@Override
	public Object call() throws Exception {
		try {
			Thread.sleep(5000);
		}catch(Exception E) {}
		return "hello";
	}
}