package day9work;



public class OneThreadOneObject {
	public static void main(String[] args) {
		new Thread(()->{
			Resource r=Factory.getResource();
			r.name="This is first thread object";
			Resource r2=Factory.getResource();
			System.out.println(r2.name);
			Resource r3=Factory.getResource();
			System.out.println(r3.name);
			Factory.removeResourceFromThread();
			Resource r4=Factory.getResource();
		}).start();
		new Thread(()->{
			Resource resource=Factory.getResource();
			System.out.println("Second thread..:"+resource.name);
			resource=Factory.getResource();
		}).start();
	}

}
class Factory{
	private static ThreadLocal tlocal=new ThreadLocal();
	public static Resource getResource() {
		Resource r=(Resource)tlocal.get();//returns current thread copy
		if(r==null) {
			r=new Resource();
			tlocal.set(r);
			return r;
		}
		else {
			return r;
		}
	}
	public static void removeResourceFromThread() {
		if(tlocal.get()!=null) {
			tlocal.remove();
		}
	}
}
class Resource{
	String name;
	public Resource() {
		System.out.println("resource cons called");
	}
}