package day9work;
//sleep method makes the thread to sleep in the monitor but doesnt allow any other thread in the monitor
//but wait allows other thread to come and work in the monitor
//notify method notifies the waiting threads.
public class OneObjectTwoThreadOneJob {
	public static void main(String[] args) {
		Gun bofors=new Gun();
		new Thread(()->{
			for(int i=0;i<5;i++)
				bofors.fill();
		},"filler").start();
		new Thread(()->{
			for(int i=0;i<5;i++)
				bofors.shoot();
		},"shooter").start();
	}
}
class Gun{
	boolean flag;
	synchronized public void fill() {
		if(flag) {
			try {
				wait();
			}catch(Exception e) {}
		}
		System.out.println("fill the gun");
		flag=true;
		notify();
	}
	synchronized public void shoot() {
		if(!flag) {
			try {
				wait();
			}catch(Exception e) {}
		}
		System.out.println("shoot the gun");
		flag=false;
		notify();
}
}