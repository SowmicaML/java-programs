package day9work;
//first main thread will execute then the other threads will race among themselves to complete their wrk
public class Threaddemo3 {
	public static void main(String[] args) {
		new Thread(()->{new Test().met();}).start();
		System.out.println("second line");
		System.out.println("second line1");
		System.out.println("second line2");
		
	}
}
class Test{
	public void met() {
		System.out.println("first line");
		try {
			Thread.sleep(5000);
		}catch(Exception e) {}
	}
}