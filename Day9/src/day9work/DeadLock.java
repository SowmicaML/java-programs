package day9work;
//refer ss in mail sub:thread coda2k20 in sent folder

public class DeadLock {
	public static void main(String[] args) {
		Frog frog=new Frog();
		Crane crane=new Crane();
		
		new Thread(()->{
			crane.eat(frog);
		}).start();
		
		new Thread(()->{
			frog.escape(crane);
		}).start();;
		
	}

}
class Frog{
	synchronized public void escape(Crane c) {
		c.CranesLeaveMethod();
		System.out.println("frog says: freeedom");
	}
	synchronized public void frogsLeaveMethod() {
		System.out.println("frog left crane");
	}
}
class Crane{
	synchronized public void eat(Frog f) {
		System.out.println("eating....lalalala");
		f.frogsLeaveMethod();
		System.out.println("swaha");
	}
	synchronized public void CranesLeaveMethod() {
		System.out.println("crane left frog");
	}
}
