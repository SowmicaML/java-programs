package convertingToExecutorframwork;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class OneObjectTwoThreadsOnejob {
	public static void main(String[] args) {
		Gun bofors=new Gun();
		ExecutorService es=Executors.newFixedThreadPool(2);
		es.execute(()->{
			Thread.currentThread().setName("filler");
			for(int i=0;i<5;i++)
				bofors.fill();
		});
		es.execute(()->{
			Thread.currentThread().setName("shooter");
			for(int i=0;i<5;i++)
				bofors.shoot();
		});
		es.shutdown();
	}
}
class Gun{
	boolean flag;
	synchronized public void fill() {
		if(flag) {
			try {
				wait();
			}catch(Exception e) {}
		}
		System.out.println("fill the gun");
		flag=true;
		notify();
	}
	synchronized public void shoot() {
		if(!flag) {
			try {
				wait();
			}catch(Exception e) {}
		}
		System.out.println("shoot the gun");
		flag=false;
		notify();
}
}