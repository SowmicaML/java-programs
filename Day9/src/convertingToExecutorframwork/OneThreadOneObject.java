package convertingToExecutorframwork;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneThreadOneObject {
	public static void main(String[] args) {
		ExecutorService es=Executors.newFixedThreadPool(2);
		es.execute(()->{
			Resource r=Factory.getResource();
			r.name="This is first thread object";
			Resource r2=Factory.getResource();
			System.out.println(r2.name);
			Resource r3=Factory.getResource();
			System.out.println(r3.name);
			Factory.removeResourceFromThread();
			Resource r4=Factory.getResource();
		});
		es.execute(()->{
			Resource resource=Factory.getResource();
			System.out.println("Second thread..:"+resource.name);
			resource=Factory.getResource();
		});
		es.shutdown();
	}
}
class Factory{
	private static ThreadLocal tlocal=new ThreadLocal();
	public static Resource getResource() {
		Resource r=(Resource)tlocal.get();//returns current thread copy
		if(r==null) {
			r=new Resource();
			tlocal.set(r);
			return r;
		}
		else {
			return r;
		}
	}
	public static void removeResourceFromThread() {
		if(tlocal.get()!=null) {
			tlocal.remove();
		}
	}
}
class Resource{
	String name;
	public Resource() {
		System.out.println("resource cons called");
	}
}