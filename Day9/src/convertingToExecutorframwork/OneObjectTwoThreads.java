package convertingToExecutorframwork;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneObjectTwoThreads {

	public static void main(String[] args) {
		ReservationCounter r=new ReservationCounter();
		ExecutorService es=Executors.newFixedThreadPool(2);
		es.execute(()->{
			synchronized(r) {
			Thread.currentThread().setName("ramu");
			r.bookTicket(1000);
			r.giveChange();}
		});
		es.execute(()->{
			synchronized(r) {
				Thread.currentThread().setName("somu");
			r.bookTicket(500);
			r.giveChange();}
		});
		es.shutdown();
		
	}
}
class ReservationCounter{
	int amt;
	 public void bookTicket(int amt) {
		this.amt=amt;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Ticket booked for..:"+name+" and amt paid is..:"+amt);
	
//		try {
//			Thread.sleep(10000);
//		}catch(Exception e) {}

	}
	synchronized public void giveChange() {
		int change=amt-100;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Change given to  :"+name+" and the change given is..:"+change);
	}
	public void drinkWater() {
		System.out.println("Drinking water..");
	}
}
