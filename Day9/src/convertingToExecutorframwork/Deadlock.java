package convertingToExecutorframwork;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Deadlock {
	public static void main(String[] args) {
		Frog frog=new Frog();
		Crane crane=new Crane();
		ExecutorService es=Executors.newFixedThreadPool(2);
		es.execute(()->{
			crane.eat(frog);
		});
		es.execute(()->{
			frog.escape(crane);
		});
		
		es.shutdown();
	}

}
class Frog{
	synchronized public void escape(Crane c) {
		c.CranesLeaveMethod();
		System.out.println("frog says: freeedom");
	}
	synchronized public void frogsLeaveMethod() {
		System.out.println("frog left crane");
	}
}
class Crane{
	synchronized public void eat(Frog f) {
		System.out.println("eating....lalalala");
		f.frogsLeaveMethod();
		System.out.println("swaha");
	}
	synchronized public void CranesLeaveMethod() {
		System.out.println("crane left frog");
	}
}
