package day2.noon;

public class Datatypes {
	//string
	//byte,short,char,int ,long
	//float and double
	//boolean
	String str="hello";
	String str2=new String ("helloo");
	byte b=120;
	char c='a';
	char cc=97;
	int i=1000;
	long lo=1010101;
	float f=1.3f;
	double d=1.3333;
	double dd=f;
	boolean boo=true;
	
	
	//wrapper class
	
	Character ch=new Character('c');
	Double dob=new Double(2929292.22);
	Long lon=new Long(212121212);
	Integer in=new Integer(1220);
	Boolean bool=Boolean.FALSE;
	
	//Type casting
	
	int num=b;
	byte byt=(byte)num;//when u do higher to lower casting,u loose precision
	
	//string value to a number
	
	String name="432";
	int nums=Integer.parseInt(name);
	
	

	
	

}
