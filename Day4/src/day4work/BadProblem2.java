package day4work;

//bidirectional association
//exercise on object state management

import java.util.Scanner;
public class BadProblem2 {
	public static void main(String[] args) {
		GoodFan kaitan=new GoodFan();
		Scanner sc=new Scanner(System.in);
		while(true)
		{
			System.out.println("enter to call pull method");
			sc.next();
			kaitan.pull();
		}
	}
}
class GoodFan{
	State state=new SwitchOffState();
	public void pull() {
		state.execute(this); //this-GoodFan obj..state is a data member
	}
}
abstract class State{
	public abstract void execute(GoodFan f);
}
class SwitchOffState extends State{
	public void execute(GoodFan f) {
		System.out.println("switch on state");
		f.state=new SwitchOnState();
	}
}
class SwitchOnState extends State{
	public void execute(GoodFan f) {
		System.out.println("medium speed state");
		f.state=new MediumSpeedState();
	}
}
class MediumSpeedState extends State{
	public void execute(GoodFan f) {
		System.out.println("High speed state");
		f.state=new HighSpeedState();
	}
}
class HighSpeedState extends State{
	public void execute(GoodFan f) {
		System.out.println("switch off state");
		f.state=new SwitchOffState();
	}
}

