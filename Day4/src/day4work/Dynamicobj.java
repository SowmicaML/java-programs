package day4work;

import java.lang.reflect.Constructor;

//dynamic binding is slower than new keyword in creating a obj.
//but we will have to trade off becoz we get to create obj dynamically.
public class Dynamicobj {
	public static void main(String[] args) throws Exception {
		
		Test t=new Test();//static way of creating objects
		
		Test t1=Test.class.newInstance();//another way of creating obj in static way
		
		Class c=Class.forName("day4work.Test");// Test.class
		
		//ther is a class called "Class".. Class.forName() method return class Class object on which we are calling
		//forName is a static method of class called "Class"
		//newInstance() method which will return the object of that class which we are passing as command line argument.
		//thts y we cast the object into type "Test"
//		Object o=c.newInstance(); //this gives error
		
		Test obj=(Test)c.newInstance(); //Test.class.newInstance() {c.newInstance()}
		obj.work();
		//in single line
		//Test obj=(Test)Class.forName("day4work.Test").newInstance();
		//or by passing arguments
		//Test obj=(Test)Class.forName(args[0]).newInstance();
		
		//FOR PARAMETERISED CONST.
		Constructor c1=Class.forName("day4work.Test").getConstructor(String.class);//string becoz we have a string parameter
		Test obj2=(Test)c1.newInstance("(with string parameter)");
		
		Constructor c2=Class.forName("day4work.Test").getConstructor(Object.class);
		Test obj3=(Test)c2.newInstance(new Object());
		
	}

}
class Test{
	public Test() {
		System.out.println("Test object created");
	}
	public Test(String s) {
		System.out.println("Test object created "+s);
	}
	public Test(Object o) {
		System.out.println(o);
	}
	public void work() {
		System.out.println("work method called");
	}
}