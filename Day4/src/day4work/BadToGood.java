package day4work;

import java.util.Scanner;

public class BadToGood {
	public static void main(String[] args) throws Exception {
		Dog tiger=new Dog();
		Child baby=new Child();
		Scanner sc=new Scanner(System.in);
		System.out.println("please enter a item name:"); //proper classname shud be given as input for eg day4work.Stone
		String itemclass=sc.next();
		Item item=(Item)Class.forName(itemclass).newInstance();
		baby.playWithDog(tiger,item);
			
	}
}
class Dog{
	
	public void play(Item item) {
		item.execute();
		
	}
}
abstract class Item{
	public abstract void execute();
}
class Stone extends Item{
	@Override
	public void execute() {
		System.out.println("You beat i bite..");
	}
}
class Stick extends Item{
	@Override
	public void execute() {
		System.out.println("You hit i ....");
	}
}

class Child {
	Item item;
	public void playWithDog(Dog dog,Item item) {
		dog.play(item);
	}
}