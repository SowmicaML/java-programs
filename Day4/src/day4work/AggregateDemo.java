package day4work;

public class AggregateDemo {
	public static void main(String[] args) {
		ShakthiSocket ss=new ShakthiSocket();
		HPPlug hp=new HPPlug();
		IndianAdapter ia=new IndianAdapter();
		ia.ap=hp;
		ss.roundpinhole(ia);
		
	}

}
abstract class IndianPlug{
	public abstract void roundpin();
}
class ShakthiPlug extends IndianPlug{
	public void roundpin() {
		System.out.println("Indian plug working..");
	}
}
abstract class IndianSocket{
	public abstract void roundpinhole(IndianPlug ip);
}

class ShakthiSocket extends IndianSocket{
	public void roundpinhole(IndianPlug ip) {
		ip.roundpin();
	}
}
abstract class AmericanPlug{
	public abstract void slabpin();
}
class HPPlug extends AmericanPlug{
	public void slabpin() {
		System.out.println("American plug working");
	}
}
class IndianAdapter extends IndianPlug{
	AmericanPlug ap;
	public void roundpin() {
		ap.slabpin();
	}
}