package day6work;

public class CorruptionDemo {
	public static void main(String[] args) {
//		Corporation c=new Corporation();
//		Hospital h=new Hospital();
//	    Police p=new Police();
	    DeathCertificate d=new DeathCertificate();
	    SevaService ser=new SevaService();
        ser.setService(1, d);
		
		ser.executeService(1);
	}
}
abstract class Command{
	
}
class Corporation{
	public void deathCertificate() {
		System.out.println("Death certificate given..");
	}
}
class Hospital{
	public void doPostMortem() {
		System.out.println("do postmortem..");
	}
}
class Police{
	public void doInvestigation() {
		System.out.println("police doing investigation");
	}
}
