package day6work;

public class SevaService {
	Service s[]=new Service[5];
	public SevaService() {
		for(int i=0;i<s.length;i++) {
			s[i]=new DummyService();
		}
	}
	
	public void executeService(int slot) {
		s[slot].execute();
	}
	public void setService(int slot,Service service) {
		s[slot]=service;
	}

}
abstract class Service{
	Corporation c;Hospital h;Police p;
	Service(){
		init();
	}
	void init() {
		c=new Corporation();
		h=new Hospital();
		p=new Police();	
		}
	public abstract void execute();
}
class DummyService extends Service{
	@Override
	public void execute() {
		System.out.println("dummy service");
	}
}
class DeathCertificate extends Service{
	@Override
	public void execute() {
		//process done by the worker - being automated...
		System.out.println("Process of issuing the death certificate started");
		p.doInvestigation();
		h.doPostMortem();
		c.deathCertificate();
		System.out.println("Work done");
	
	}
}