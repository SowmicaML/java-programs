package day6work;

import java.util.Scanner;

public class CustomException {
	public static void main(String[] args) {
		TestCustomException te=new TestCustomException();
		try {
			te.test();
		}catch(Throwable t) {
			System.out.println(t);
		}
	}
}
class TestCustomException{
	public void test() throws Throwable{
		//1.skips the compile time check for exception handling
		//2.it conveys to the caller that this method is capable of throwing an exception
		//3.it ensures that the calling method handles or throw the exception
		//4.THis scenario is called as handling-CHECKED EXCEPTIONs
		Scanner scan=new Scanner(System.in);
		System.out.println("Input some value");
		String in=scan.next();
		if(in.equals("ramu")) {
			throw new MyException("This is custom exception..");//throw is to throw an exception
		}
	}
}
class MyException extends Throwable{
	String msg;
	public MyException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "The exception msg is "+msg;
	}
}