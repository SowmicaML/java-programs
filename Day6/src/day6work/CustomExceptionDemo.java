package day6work;
import java.util.Scanner;

public class CustomExceptionDemo {
	public static void main(String[] args) throws Exception{ 
		Child baby=new Child();
		Dog tiger=new Dog();
		Scanner scan=new Scanner(System.in);
		System.out.println("Please enter item class name..:");
		String itemclass=scan.next();
		Item item=(Item)Class.forName(itemclass).newInstance();
		baby.playWithDog(tiger, item);
	}
}
class Child{
	public void playWithDog(Dog dog,Item item) {
		try{
			dog.play(item);
		}
		catch(DogExceptions dee) {
			dee.visit();
		}
	}
}
class Handler911{
	public void handle(DogBiteException dbe) {
		System.out.println("take him to hospital...."+dbe);
	}
	public void handle(DogBarkException dre) {
		System.out.println("no worries............just ignore..."+dre);
	}
	public void handle(DogHappyException dhe) {
		System.out.println("let us play with dog......"+dhe);
	}
}
class Dog{
	public void play(Item item)throws DogExceptions {
		item.execute();
	}
}
abstract class Item{
	public abstract void execute()throws DogExceptions;
}
class Stick extends Item{
	@Override
	public void execute()throws DogExceptions {
		throw new DogBiteException("you beat I bite....");		
	}
}
class Stone extends Item{
	@Override
	public void execute() throws DogExceptions{
		throw new DogBarkException("you throw I bark.............")		;
	}
}
class Biscuit extends Item{
	@Override
	public void execute() throws DogExceptions{
		throw new DogHappyException("yummy yummy I love biscuits....")		;
	}
}
//how to write custom exception
abstract class DogExceptions extends Exception{
	private static Handler911 h911;
	public static Handler911 getH911() {
		return h911;
	}
	static {
		h911=new Handler911();
	}
	public abstract void visit();
}
class DogBiteException extends DogExceptions{
	private String msg;
	public DogBiteException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
	@Override
	public void visit() {
		getH911().handle(this);
	}
}
class DogBarkException extends DogExceptions{
	private String msg;
	public DogBarkException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
	@Override
	public void visit() {
		getH911().handle(this);		
	}
}
class DogHappyException extends DogExceptions{
	private String msg;
	public DogHappyException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
	@Override
	public void visit() {
		getH911().handle(this);		
	}
}
