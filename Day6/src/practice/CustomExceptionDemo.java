package practice;
import java.util.Scanner;

public class CustomExceptionDemo {
	public static void main(String[] args)throws Exception {
		Dog tiger=new Dog();
		Child baby=new Child();
		System.out.println("Please enter the item name");
		Scanner scan=new Scanner(System.in);
		String itemclass=scan.next();
		Item item=(Item)Class.forName(itemclass).newInstance();
		baby.playwithDog(tiger, item);
		

	}
}
class Child{
	public void playwithDog(Dog dog,Item item) {
		dog.play(item);
	}
}
class Dog {
	public void play(Item item) {
		try{item.execute();
		}catch(DogException de) {
			de.visit();
		}
	}
}
class Handler911{
	public void handle(DogBiteException de) {
		System.out.println("Take the child to hosp"+"Reason:"+de);
	}
	public void handle(DogBarkException db) {
		System.out.println("Ignore! "+"Reason:"+db);
	}
	public void handle(DogHappyException dh) {
		System.out.println("Play with dog! "+"Reason"+dh);
	}
}

abstract class Item{
	abstract public void execute() throws DogException;
}
class Stone extends Item{
	public void execute() throws DogException {
		throw new DogBiteException("Dog bit the kid");
	}
}
class Stick extends Item{
	public void execute() throws DogException {
		throw new DogBarkException("Dog barked at the kid");
	}
}
class Biscuit extends Item{
	public void execute() throws DogException {
		throw new DogHappyException("Dog is very happy");
	}
}
abstract class DogException extends Exception{
	private static Handler911 h;
 {
		h=new Handler911();
	}
    public Handler911 geth() {
	   return h;
    }
	abstract public void visit();
}
class DogBiteException extends DogException{
	private String msg;
	public DogBiteException(String msg) {
		this.msg=msg;	
	}
	
	@Override
	public String toString() {
		return msg;
	}
	public void visit() {
		geth().handle(this);
	}
}
class DogBarkException extends DogException{
	private String msg;
	public DogBarkException(String msg) {
		this.msg=msg;	
	}
	
	@Override
	public String toString() {
		return msg;
	}
	public void visit() {
		geth().handle(this);
	}
}class DogHappyException extends DogException{
	private String msg;
	public DogHappyException(String msg) {
		this.msg=msg;	
	}
	
	@Override
	public String toString() {
		return msg;
	}
	public void visit() {
		geth().handle(this);
	}
}
