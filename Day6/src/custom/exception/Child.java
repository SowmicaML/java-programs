package custom.exception;

public class Child {
public void playWithDog(String item,Dog dog) {
	try {
		dog.play(item);
	}catch(DogExceptions de) {
		de.visit();
	}
}
}