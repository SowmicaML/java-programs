package Day3work;

public class Democlass4 {
	
	public static void main(String[] args) {
		Human sowmi =new Human();
		System.out.println(sowmi);
		new Human("sowmi");
		new Human(12);
	}

}
class Human{
	public Human() {
		System.out.println("crying..");
	}
	public Human(String s)
	{
		System.out.println("with parameters");
		
	}
	public Human(int i)
	{
		System.out.println("number constructor");
	}
	
	@Override
	public String toString() {
		return "Human [getClass()=" + getClass() + ",hashCode()=" + hashCode() + ", toString()=" +super.toString()+"]" ;
	}
}