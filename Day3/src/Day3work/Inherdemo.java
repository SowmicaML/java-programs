package Day3work;
//PURPOSE OF INHERITANCE
//polymorphic query
//part whole hierarchy
//composition
//removal of if else if
//object reusability
//core reusability

public class Inherdemo {
	public static void main(String[] args) {
		PaintBrush brush=new PaintBrush();
		brush.paint=new Pinkpaint();
		brush.dopaint();
	}

}
class PaintBrush{
    Paint paint;
	public void dopaint () {
		System.out.println(paint);	
	}
}
class Paint{}
class Redpaint extends Paint{}
class Bluepaint extends Paint{}
class Greenpaint extends Paint{}
class Pinkpaint extends Paint{}

//STRATEGY PATTERN
//formula to eliminate if-else-if ladder
//1.Delete if-else-if throw the code in kuppai
//2.convert the condition to classes
//3.group them under a heirarchy..
//4.create associaition between the using class the top hierarchy class