package Day3work;

public class OverloadDemo {
    public static void main(String[] args) {
    	Help911 H =new Help911();
    	H.help(new Domestic("Brothers are fighting in the street"));
    	H.help(new Theft("Theft in neighbourhood"));
	}
	
}
class Help911 {
	public void help(Theft t) {
		System.out.println("Theft prb method called :" +t);
		
	}
	public void help(Medical m) {
		System.out.println(m);
		
	}
	public void help(Domestic d) {
		System.out.println("Domestic prb method called.."+d);
	}
}
class Theft {
	String msg;
	public Theft(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "THeft prb is..."+this.msg;
	}
}
class Medical{
	String msg;
	public Medical(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "medical prb is..."+this.msg;
   }
}
class Domestic {
	String msg;
	public Domestic(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "Domestic prb is..."+this.msg;
   }
}