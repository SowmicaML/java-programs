package Day3work;

public class Overridedemo {
	public static void main(String[] args) {
//		Child parent=new Parent(); //error
		Parent parent =new Child();
		parent.met();
		
	}

}
///constructors cannot be overridden
//visibility cannot be reduced   (public>protected>no mod>private)
//return type cannot be changed
//name and method parameters cannot be changed     //for overriding
class Parent{
	Parent(){
		System.out.println("Non parameter cons");
	}
	Parent(int i)
	{
		System.out.println("Parent constructor");
	}
	public void met() {
		System.out.println("Parent class");
	}
}
class Child extends Parent {
	Child(){
		super(23);
		System.out.println("Child constructor called");
	}
	public void met() {
		super.met();
		System.out.println("Child class");
	}
}