package Day3work;

public class Staticdemo {
	public static void main(String[] args) {
		SingleTon st1=SingleTon.createSingleTon();//static methods are invoked by classname.staticmethodname
		SingleTon st2=SingleTon.createSingleTon();
		
		MultiTon mt1=new MultiTon();
		MultiTon mt2=new MultiTon();
		
	}

}
class SingleTon{
	private SingleTon() {
		System.out.println("singleton created");
	}
	private static SingleTon single;
	public static SingleTon createSingleTon() { //this is done inorder to create the singleton obj only once.
		if(single==null) {
			single=new SingleTon();
		}
		return single;
	}
}
class MultiTon{
	public MultiTon() {
		System.out.println("Multiton onject created");
	}
}