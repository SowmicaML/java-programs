package day7work;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
public class InterfaceSubjection{
public static void main(String[] args) {
	AlopathyMedicalCollege stanley=new AlopathyMedicalCollege();
	AyurvedaMedicalCollege ayush=new AyurvedaMedicalCollege();
	JetAcademy jet=new JetAcademy();
	
	//Human shoiab=new Human();
	//subjection
	//Doctor doctorshoiab=
		//	(Doctor)Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class}, new MyInvocationHandler(new Object[] {stanley}));
	
//	doctorshoiab.doCure();
	
	Object object=Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class,Pilot.class},
			new MyInvocationHandler(new Object[] {stanley,jet}));
	
//explaination:inorder to dynamically add a new behaviour to the object we use this concept for eg..human is born..he learns engg..he wants to be singer
//so he cant restart his life so inorder to continue it as such we use this concept
//in proxy instance we have parameters.first one is for class object..2nd one is interfaces..3rd one is objects
//we have created a object of class Object using proxy so we need to cast it to the class type we require
//then we invoke the method say dr.docure..so internally it checks for docure method in classes of object stanley and jet
//that is wh we have the for loop to check the method in the classes
//the method that is found first will be executed say if we have docure in both doctor and jetacademy class then docure in doctor class will be execute first
//so it is imp to note that we dont have methods repeated..its not an issue but still it can be avoided.
//doctorshoiab.doCure();-----This automatically invokes the "invoke of method class"..tht does the searching job and executes the function
	
	
	Doctor doctorshoiab=(Doctor)object;
	Pilot pilotshoiab=(Pilot)object;
	doctorshoiab.doCure();
	
	pilotshoiab.fly();	
}
}
class MyInvocationHandler implements InvocationHandler{
	Object obj[];
	Object o;
	public MyInvocationHandler(Object obj[]) {
		this.obj=obj;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		for(int i=0;i<obj.length;i++) {
			try {
			 o=method.invoke(obj[i], args);
			}catch(Exception e) {}
		}
		return o;
	}
}
interface Doctor{
	public void doCure();
	public void doGiveMedicine();
}
interface Nurse{
	public void nursing();
}
interface Pilot{
	public void fly();
}
interface Steward{
	public void serve();
}
class AlopathyMedicalCollege implements Doctor,Nurse{
	@Override
	public void doCure() {
		System.out.println("alopathy cure for corona started...");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("no medicine in alopathy so give inji morabba....");
	}
	@Override
	public void nursing() {
		System.out.println("nursing course...........nurse work done...");
	}
}
class AyurvedaMedicalCollege implements Doctor{
	@Override
	public void doCure() {
		System.out.println("ayurved cure for corona started...");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("kabasura neer given to cure.......");
	}
}
class JetAcademy implements Pilot,Steward{
	@Override
	public void fly() {
		System.out.println("learning to fly aeroplane............");
	}
	@Override
	public void serve() {
		System.out.println("become a aeroplane velakaaran....");		
	}
}
class Human{
	
}





