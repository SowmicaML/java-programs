package day7work;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class HeirarchyFieldMehtodClassDemo {
	public static void main(String[] args) throws Exception {
		Gmail g=new Gmail();
		Internet i=new Internet();
		i.UseAccount(g);
		
	}

}
class Internet{
public void UseAccount(Account a)throws Exception {
	Class c=a.getClass();
	Field field=c.getField("username");
	System.out.println(field);
	
	
	Field field1=c.getDeclaredField("password");
	field1.setAccessible(true);
	System.out.println(field1.get(a));
	
	Method met=c.getMethod("login");
	met.invoke(a);	
  }
}
abstract class Account{
	abstract public void login();
}
class Gmail extends Account{
	public String username="sowmica";
	private String password="sow123";
	@Override
	public void login() {
		System.out.println("Gmail accnt logged in");
	}
}
class Yahoo extends Account{
	public String username="priya";
	private String password="sow12345678";
	@Override
	public void login() {
		System.out.println("Yahoo accnt logged in");
	}
}