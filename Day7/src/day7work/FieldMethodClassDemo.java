package day7work;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodClassDemo{
	public static void main(String[] args) throws Exception {
		PoliceStation p1=new PoliceStation();
		Politician ramu=new Politician();
		Naxalite nax=new Naxalite();
		p1.arrest(ramu);
	}
}
class PoliceStation{
	public void arrest(Object p) throws Exception {
		//introspection..//reflection
		Class c=p.getClass();  //getClass() is the method of Object class. This method returns the runtime class of this object.
		Field field=c.getField("name");//The getField() method of java Class class returns a field object which represents the 
		//public member field of the class or interface represented by this Class object
		System.out.println(field.get(p));
		Field fields[]=c.getFields();//method of java Class class returns a method object representing the specified public member
		//method of the class or interface represented by this Class object. The name parameter is passed as a string.
		for(Field f:fields) {
			System.out.println(f.getName());
		}
//		
//		Method met=c.getMethod("work",new Class[] {String.class});
//		met.invoke(p, new Object[] {"hello world"});
		Method met=c.getMethod("work", new Class[] {String.class});
		met.invoke(p, new Object[] {"hello world"} );
		
		Method methods[]=c.getMethods();
		for(Method m:methods) {
			System.out.println(m.getName());
		}
		tortureRoom(p);
	}
	public void tortureRoom(Object p)throws Exception {
		Class c=p.getClass();
		Field field=c.getDeclaredField("secretName");//getDeclaredField is for all kind of specifiers//getField is only for public modifiers
		field.setAccessible(true);
		System.out.println(field.get(p));
		
		Method met=c.getDeclaredMethod("secretWork", new Class[] {String.class});
		met.setAccessible(true);
		met.invoke(p,new Object[] {"aaaa"});
	}
	
}



class Naxalite {
	public String name="i am naxalite..";
	private String secretName="naxi";
	public void work(String s) {
		System.out.println("nax says "+s);
	}
	
}
class Politician {
	public String name="I am Good Holy Man";
	private String secretName="gunda...rowdy...dash dash...";
	
	public void work(String s) {
		System.out.println("I do social work.....very good person...I am noble....I am for people and .....");
	}
	
	private void secretWork(String s) {
		System.out.println("murder....dacoit....loot...arson....dash dash.......");
	}
	
}