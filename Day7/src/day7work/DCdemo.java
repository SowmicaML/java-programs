package day7work;

//A Soft reference is eligible for collection by garbage collector, but probably won't be collected until its memory is needed. i.e. garbage collects before OutOfMemoryError.
//
//A Weak reference is a reference that does not protect a referenced object from collection by GC. i.e. garbage collects when no Strong or Soft refs.

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

public class DCdemo {
	public static void main(String[] args) {
		Runtime r=Runtime.getRuntime();
		System.out.println("Before thatha's Birth:"+r.freeMemory());
		GrandFather thatha=new GrandFather();
		System.out.println("After thatha's birth:"+r.freeMemory());
//		SoftReference<GrandFather> soft=new SoftReference<GrandFather>(thatha);//soft ref removes obj frm memory but we can call again.
		WeakReference<GrandFather> weak=new WeakReference<GrandFather>(thatha);
		thatha=null;
		System.out.println("After thatha's death:"+r.freeMemory());
		System.out.println("kariam");
		r.gc();//in reality u dont call gc,rather it is called when it is needed by jvm
		System.out.println("After kariam:"+r.freeMemory());
		thatha=weak.get();
		System.out.println(thatha.age);
	
	}
}
class GrandFather{
	String age;
	private String gold="UNDER THE TREE";
	public GrandFather() {
		for(int i=0;i<10000;i++) {
			age=new String(i+"");
		}
	}
	private String getGold() {
		return "The gold id..:"+gold;
	}
	@Override
	protected void finalize() throws Throwable {
		System.out.println("finalized called..The gold is "+getGold());
	}
}