package day7work;

public class PrototypeDemo {
	public static void main(String[] args) {
		//first scenario
		System.out.println("first scenario");
		Sheep s1=new Sheep();
		Sheep s2=new Sheep();
		
		s1.name="mother";
		s2.name="duplicate";
		System.out.println(s1.name+" : "+s2.name);
		
		//second scenario
		System.out.println("Second scenario");
		Sheep sh1=new Sheep();
		Sheep sh2=sh1;
		sh1.name="mother mother";
		sh2.name="dup";
		System.out.println(sh1.name+" : "+sh2.name);
		
		//third
		System.out.println("Third scenario-clone/proto-Resources are shared but properties are unique"
		+"one object with 2 references but data is unique");
	     Sheep mothersheep=new Sheep();
	     Sheep dolly=mothersheep.createProto();
	     mothersheep.name="iam mother sheep..";
	     dolly.name="i am dolly clone..";
	     System.out.println(mothersheep.name+" :"+dolly.name);
	     }

}
class Sheep implements Cloneable{
	String name;
	public Sheep() {
		System.out.println("sheep cons called");
 }
	public Sheep createProto() {
		try {
			return (Sheep)super.clone();
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}