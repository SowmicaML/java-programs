package day8work;

public class SingleThreadDemo {
	public static void main(String[] args) throws Exception {
		Thread t=Thread.currentThread();;
		t.setName("Sowmi");
		t.setPriority(10);//priority is from 1 to 10(10 is highesst prop and 0 is lowest)
		System.out.println(t);
		for(int i=0;i<5;i++) {
			System.out.println(i);
			Thread.sleep(1000);//control check val
			//System.exit(1)
			
		}
		//how to span a new thread
		Thread t2=new Thread(()->{
			System.out.println("child thread1");
			});
		Thread t3=new Thread(new MyRunnable());//traditional -third
//		System.out.println(t2);
//		System.out.println(t3);
		new Thread(new Runnable() {//anonymous inner classes-second
			@Override
			public void run() {
				System.out.println("child thread2");
				
			}
		}).run();
	}

}
class MyRunnable implements Runnable{
	@Override
	public void run() {
		System.out.println("child thread3..");
	}
}