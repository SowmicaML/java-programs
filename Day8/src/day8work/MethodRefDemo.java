package day8work;

public class MethodRefDemo {
	public static void main(String[] args) {
		Func1 fadd=new AddBazar()::add;
		int sum=fadd.add(10, 20);
		System.out.println("Sum is..:"+sum);
		
		Func1 faddstat=AddBazar::addstatic;
		int sum2=faddstat.add(10, 50);
		System.out.println("sums is :"+sum2);
	}
}
interface Func1{
	public int add(int i,int j);
//	public int addd(int i,int j);
}
class AddBazar{
	public int add(int i,int j) {
		return i+j;
	}
	public static int addstatic(int i,int j) {
		return i+j;
	}
}