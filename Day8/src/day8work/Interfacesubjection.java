package day8work;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


public class Interfacesubjection {
	public static void main(String[] args) {
		Human sowmi=new Human();
		AlopathyMedicalCollege a=new AlopathyMedicalCollege();
		JetAcademy jet=new JetAcademy();
		Object object=Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Pilot.class, Doctor.class},new MyInvocationHandler(new Object[] {a,jet}));
		Doctor drsow=(Doctor)object;
		Pilot pilotsow=(Pilot)object;
		drsow.doCure();
		
		pilotsow.fly();
		}
}
class MyInvocationHandler implements InvocationHandler{
	Object obj[];
	Object o;
	public MyInvocationHandler(Object obj[]) {
		this.obj=obj;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		for(int i=0;i<obj.length;i++) {
			try {
			 o=method.invoke(obj[i], args);
			}catch(Exception e) {}
		}
		return o;
	}
}
interface Doctor{
	public void doCure();
	
}
interface Pilot{
	public void fly();
}
class AlopathyMedicalCollege implements Doctor{
	@Override
	public void doCure() {
		System.out.println("alopathy cure started...");
	}	
}
class JetAcademy implements Pilot{
	@Override
	public void fly() {
		System.out.println("learning to fly ");
	}
}
class Human{
	
}
