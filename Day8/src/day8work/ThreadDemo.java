package day8work;
//start starts the thread tht is the thread is ready to work
//run means tht thread has strted to work
//runnable is a work assigned to the thread ..it has void method
//callable is same as runnable but it has return method
//this is 1991 wala concept..now thread can only be created in 2 ways 1)thread2)executive framework

public class ThreadDemo {
	public static void main(String[] args) {
		new Thread(()->{
			System.out.println("child thread called..");
			
		}).run();
		System.out.println("main thread");
	}

}
