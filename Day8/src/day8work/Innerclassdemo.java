package day8work;

public class Innerclassdemo {

}
//abstract class with no function-marker class
//abstract class with single function-functional abstract class
//you should go with anonymous class when you have functional abstract class
//you shoud go with lambda functions when u hv functional interface.
abstract class Cola{
	public abstract void makeCola();
}

class Pepsi{
	public void makePepsi() {
//		class Campacola extends Cola{//local inner class
//			@Override
//			public void makeCola() {
//				System.out.println("cola made by campa cola..");
//			}
//		}
//		Cola cola=new Campacola();
//		cola.makeCola();
		
		//anonymous inner class    //do not create anonymous class for abstract class with more than one method..its useless..so make sure abstract class has only one method.
		new Cola() {
			
			@Override
			public void makeCola() {
				System.out.println("cola made...");
			}
		}.makeCola();
		System.out.println("fill in pepsi bottle sell");
	}
//	public Cola trojan(){
//	   return new Campacola();
//	}
}
class KaliMark{
	public void makeBovonto() {
		//Cola cola=new Pepsi().new CampaCola();
		//Cola cola=new Pepsi.CampaCola();//access inner class
//		Cola cola=new Pepsi().trojan();
//		cola.makeCola();
//		System.out.println("Fill in bovonto bottle sell..");
	}
}