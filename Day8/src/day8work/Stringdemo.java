package day8work;

public class Stringdemo {
	public static void main(String[] args) {
		Demo d=new Demo();
		d.exploreString();
	}
}
class Demo{
	public void exploreString() {
		String s1="hello world";
		String s2="hello world";
		System.out.println(s2);
		System.out.println(s1);//string isnon mutable obj..
		
		String str=new String("hello");
		String str2=new String("hello");
		
		System.out.println(str2);
		System.out.println(str);
		//equals-compares two string
		System.out.println(str.equals(str2));
		
		System.out.println(str.charAt(2));
		
		char c[]= {'a','b','c'};
		String cstr=new String(c,0,1);
		System.out.println(cstr);
		//concatenate never use string
		//instead make use of StringBuffer or String Builder
		//String Buffer-old class -prior to jdk5-thread safe-slow
		//String Builder-new class -introduced in jdk5-non thread safe-fast
        StringBuilder sb1=new StringBuilder("hello");//synchronized-sequentia-slower-OLD
        sb1.append("world");
        
        StringBuffer sbf=new StringBuffer("hello");//non-synchronized-random-faster-new
        sbf.append("world");
        
        String sf=String.format("The String vlaue is..%s and integer value is..%d"," hello world",200);
        System.out.println(sf);
        System.out.printf("The String vlaue is..%s and integer value is..%d"," hello world",200);
	    System.out.println();
        work(1,2,3,100,111);
        
        System.out.printf("Integer :%d\n",15);
        System.out.printf("Floating point number with 3 decimal digits:%.3f\n",1.2131543573567);
        System.out.printf("Floating point number with 3 decimal digits:%.8f\n",1.2131657576777767);
	    String s=String.format("String :%s,integer :%06d,float:%.6f","Hello",89,9.435345);
	    System.out.println("\n"+s);
	    System.out.printf("%-12s%-12s%s\n","Column 1","Column 2","Column3");
	    System.out.printf("%-12.5s%s","Helsdflo World","HEllo World");
	}
	public void work(int ...a) {//var args
		for(int i:a) {
			System.out.println(i);
		}
	}
}