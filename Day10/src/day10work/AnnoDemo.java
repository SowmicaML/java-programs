package day10work;

import java.lang.reflect.Field;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class AnnoDemo {
	public static void main(String[] args) throws Exception {
		Tea tea=new Tea();
		Field f=tea.getClass().getDeclaredField("sugar");
		f.setAccessible(true);
		NoSugar ns=f.getAnnotation(NoSugar.class);
		if(ns!=null) {
		}
		else {
			tea.sugar=new Sugar();
		}
		System.out.println(tea.sugar);
	}
}
class Tea{
	@NoSugar
	Sugar sugar;
}
class Sugar{
	
}
@Retention(RetentionPolicy.RUNTIME)
@interface NoSugar{}