package day10work;

import java.util.ArrayList;
public class GenericDemo {
	public static void main(String[] args) {
		PaintBrush<Paint> pb=new PaintBrush<>();
//		pb.setObj(new RedPaint());
//		Paint paint=pb.getObj();
//		paint.doColour();
		
		ArrayList<Integer> al=new ArrayList<>();
		al.add(3444);
		al.add(2333);
		al.add(1223);
		for(int i:al) {
			System.out.println(i);
		}
		
	}
}
class PaintBrush<T>{
	private T obj;
	public T getObj() {
		return obj;
	}
	public void setObj(T obj) {
		this.obj = obj;
	}	
}
abstract class Paint{
	abstract void doColour();
}
class RedPaint extends Paint{
	@Override
	void doColour() {
		System.out.println("red color...");	
	}
}
abstract class Liquid{
	abstract void spray();
}
class ColourLiquid extends Liquid{
	@Override
	void spray() {
		System.out.println("holi holi holi......re....");
	}
}
abstract class Dry{
	abstract void gijiGija();
	abstract void dusting();
}
class DryAir extends Dry{
	@Override
	void dusting() {
		System.out.println("i clean my laptop.....key board....");
	}
	@Override
	void gijiGija() {
		System.out.println("I play kakabuka kakabuka......");		
	}
}

