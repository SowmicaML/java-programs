package day10work;


public class Enumdemo2 {
	enum Directions{
		NORTH,EAST,WEST,SOUTH;
		public void met() {
			System.out.println("this is a method ");
			
		}
		public void met(int n) {
			System.out.println("method overloadd");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Directions e;
		e=Directions.EAST;
		e.met();
		e.met(8);
		Info info=Info.NAME;
		System.out.println(info.get());
		for(Info i:Info.values()) {
			System.out.println(i.get()+"  "+i.ordinal());
		}
	}
}
enum Info{
	NAME("ramu"),FATHER("SOMU"),DOB("18-7-1999");
	private String a;
	private Info(String a) {
		this.a=a;
	}
	public String get() {
		return a;
	}
	
}