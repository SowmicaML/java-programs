package day10work;

import java.util.Scanner;

public class EnumDemo3 {
	enum Operations{
		ADD{
			@Override
			int execute(int n1, int n2) {
				// TODO Auto-generated method stub
				return n1+n2;
			}
		},
		SUBTRACT{
			@Override
			int execute(int n1, int n2) {
				// TODO Auto-generated method stub
				return n1-n2;
			}
		},
		MULTIPLY{
			@Override
			int execute(int n1, int n2) {
				// TODO Auto-generated method stub
				return n1*n2;
			}
		},
		DIVISION{
			@Override
			int execute(int n1, int n2) {
				// TODO Auto-generated method stub
				return n1/n2;
			}
		};
		
		abstract int execute(int n1,int n2);
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n1,n2;
		System.out.println("Enter the num1");
		Scanner sc=new Scanner(System.in);
		n1=sc.nextInt();
		System.out.println("Enter the num2");
		n2=sc.nextInt();
		System.out.println("Enter Operation");
		String opr=sc.next();
		Operations op=Operations.valueOf(opr.toUpperCase());
		System.out.println(op.execute(n1, n2));
		/**
		switch(op) {
		case ADD:
			System.out.println("THE ADDITION "+(n1+n2));
			break;
		case SUBTRACT:
			System.out.println("THE SUBTRACT "+(n1-n2));
			break;
		case MULTIPLY:
			System.out.println("THE MULTIPLY "+(n1*n2));
			break;
		case DIVISION:
			System.out.println("THE DIVISION "+(n1/n2));
			break;
		default:
			System.out.println("INVALID OPERATION");
		}**/
		
		
	}
}