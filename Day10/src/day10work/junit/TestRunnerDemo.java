package day10work.junit;

import java.util.List;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

//junit runner package,   result-> info related to tests   -> runClasses() method of junitcore
public class TestRunnerDemo {

	public static void main(String[] args) {
		
		//object-type Result
		
		Result   rslt=	JUnitCore.runClasses(JunitFirstDemo.class ,  AnnotationsDemo.class);
		
		System.out.println(rslt.getFailureCount());
		
		List <Failure> lis= rslt.getFailures();
		
		for( Failure i: lis)
		{
			System.out.println(i.toString());
		}
		
		System.out.println(rslt.getRunTime());
		
		System.out.println(rslt.wasSuccessful()); 
		
		System.out.println(rslt.getRunCount());
		
		
	}

}
