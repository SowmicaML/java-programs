package day10work.junit;

import static org.junit.Assert.*;
import org.junit.Test;
/*
junit 4-> open source framework for unit testing......testing a single class or suite of classes
​
Rules:  1.public 2.annoted @test     3.return type->void
 
@Test -> specifies that method is the test method.
@Test(expected=Exception.class) -> If it doesn't throw an exception or if it throws a different exception than the one declared, the test fails.
@Test(timeout=1000) -> specifies that method will be failed if it takes more than specified time.   */

//Failures notify of an invalid test result, errors indicate an unexpected situation (exception)

public class JunitFirstDemo{ 
	
		Calculator c = new Calculator();
		@Test
		public void add2()
		{
			assertEquals("Manipulation error",15, c.add(10, 5));
		}
		
		@Test(timeout=500)
		public void timeCheck()
		{
			//System.out.println("hello...");
			//for(;;);
		}
		
		@Test(expected= ArithmeticException.class)
		public void checkDivideByZero()
		{
			System.out.println(5/0);
		}	
}
class Calculator
{
	public int add(int x,int y)
	{
		return x+y;
	}
	public int sub(int x,int y)
	{
		return x-y;
	}
}
