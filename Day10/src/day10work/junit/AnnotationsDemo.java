package day10work.junit;

//import static org.junit.Assert.*;
/* In Junit 4.x .... @Before -> setUp() method and @After -> tearDown() method of JUnit 3.x
 to initialize and clean up any common objects  ->  fixtures.  
 annotations -> additional info/ metadata to jvm  */

import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.junit.Test;

/*   @Ignore -> A String parameter can be added to define the reason for ignoring       (�Not Ready to Run�)
    @Test        @Test(expected=Exception.class)      @Test(timeout=ms) 

@BeforeClass -> specifies that method will be invoked only once, before starting all the tests.
@Before -> specifies that method will be invoked before each test.
@After -> specifies that method will be invoked after each test.
@AfterClass -> specifies that method will be invoked only once, after finishing all the tests.
*/


public class AnnotationsDemo 
{ Calculator c=null;
 
  @Before
	public void before()
	{   c=new Calculator();
		System.out.println("before test.....");
	}
  @After
	public void after()
	{
		System.out.println("after test.....");
	}
  @BeforeClass
	public static void beforeClass()
	{
		System.out.println("before class.....");
	}
  @AfterClass
	public static void afterClass()
	{
		System.out.println("after class.....");
	}
	
  @Ignore ("not needed now")
	@Test
	public void add2()
	{    System.out.println("add method...");
		 assertEquals("Manipulation error",15, c.add(10, 5) );
	}
	@Test
	public void sub2()
	{	System.out.println("sub method...");
		 assertEquals("Manipulation error",5, c.sub(10, 5) );
	}
}
