package day10work.junit;

import static org.junit.Assert.*;
import org.junit.Test;

public class AssertionDemo 
{
	@Test
	public void check()
	{
		Account one = new Account(200, "cust one");
        Account two = new Account(200, "cust two");
        Account three = new Account(100, "cust three");
        Account four = null;
        Account five = one;
        Account[] group1 = { one, two, three };
        Account[] group2 = { one, two, five};
        
        // assertTrue - checking if a condition is true
        assertTrue(one.getBalance() == two.getBalance());
        	 
        // assertFalse - checking if a condition is false
        assertFalse(one.getBalance() == three.getBalance());
        	 
        // assertNull - checking if an object is null
        assertNull(four);
        	 
        // assertNotNull - checking if an object is not null
        assertNotNull(three);
        	 
        // assertEquals - checking if two objects are equal
        assertEquals(one.getName(),three.getName());
        	 
        // assertSame - checking if two objects references point the same object
        assertSame(one, five);
        	 
        // assertNotSame - checking if two objects references don't point the same object
        assertNotSame(four, five);
        	 
        // assertArrayEquals - checking if two arrays are the equal
         assertArrayEquals(group1, group2);

	}
}

class Account
{
	private double balance;
    private String name;
 
    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }
 
    public double getBalance() {
        return balance;
    }
 
    public String getName() {
        return name;
    }
}
