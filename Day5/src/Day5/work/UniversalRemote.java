package Day5.work;

public class UniversalRemote {
	Command c[]=new Command[5];
	public UniversalRemote() {
		for(int i=0;i<c.length;i++) {
			c[i]=new DummyCommand();
		}
	}
	
	public void executeCommand(int slot) {
		c[slot].execute();
	}
	public void setCommand(int slot,Command command) {
		c[slot]=command;
	}
}
abstract class Command{
	Tv tv;SetTopBox st;NetFlix nf;SoundSystem ss;AmazonPrime ap;
	public Command() {
		init();
	}
	void init() {
		tv=new Tv();st=new SetTopBox();nf=new NetFlix();ss=new SoundSystem();ap=new AmazonPrime();
	}
	public abstract void execute();
}
class DummyCommand extends Command{
	@Override
	public void execute() {
		System.out.println("I am dummy yet to be operational....");
	}
}
class SerialChannelCommand extends Command{
	@Override
	public void execute() {
		//process done by the worker - being automated...
		System.out.println("serial channel work started.....");
		tv.switchOn();
		tv.av1Mode();
		st.serialChannel();
		ss.lowSound();
		System.out.println("enjoy the serial....");
	}
}
class NewsChannelCommand extends Command{
@Override
	public void execute() {
	System.out.println("news channel work started.....");
	tv.switchOn();
	tv.av1Mode();
	st.newsChannel();
	ss.highSound();
	System.out.println("enjoy the news....");
		
	}	
}
